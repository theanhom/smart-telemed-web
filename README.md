# Smart Queue TrakanHospital

### generate routing

```shell
ng g module nyan --module app --route nyan
```

### Create service

```
npx ng g s features/nurse/services/lib --skip-test
```
<!-- รันโปรเจค web -->
npx ng serve
<!-- รันโปรเจค web เปิด Browser เลย -->
npx ng serve -o
<!-- สร้าง page โมดูล -->
ng g module features/admit-center --module app --route admit-center
<!-- สร้าง Sub โมดูลใน page -->
ng g module features/admit-center/main --module features/admit-center --route main
<!-- สร้าง service guard -->
ng g s shared/guard/admit-center-guard
<!-- สร้าง guard -->
ng g g core/guard/admit-center
<!-- สร้าง component -->
ng g c features/register/patient-list/print-fu
