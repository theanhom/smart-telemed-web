# Stage 1
FROM node:18-alpine as builder

LABEL maintainer="Niphon  Theanhom <niphon.theanhom@gmail.com>"

WORKDIR /app

RUN apk add --upgrade --no-cache --virtual deps python3 build-base git

COPY . .

RUN npm i
RUN npm run build

# STAGE 2
FROM nginx:stable-alpine

COPY --from=builder /app/dist/web /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
