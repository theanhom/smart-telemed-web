import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // pathPrefixLookup: any = `:40013/lookup`
  // pathPrefixNurse: any = `:40012/nurse`
  // pathPrefixAuth: any = `:40010/auth`
  pathPrefixLookup: any = environment.pathPrefixLookup;
  pathPrefixNurse: any = environment.pathPrefixNurse;
  pathPrefixAuth: any = environment.pathPrefixAuth;

  private axiosInstance = axios.create({
    // baseURL: `${environment.apiUrl}${this.pathPrefixAuth}`
    baseURL: `${this.pathPrefixNurse}`
  });

  constructor() {
    this.axiosInstance.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })
  }

  async login(username: any, password: any) {
    const url = `/login/login`;
    return this.axiosInstance.post(url, {
      username, password
    });
  }

  async checkLogin(username: any, password: any) {
    const url = `/loginhim/checklogin`;
    return this.axiosInstance.post(url, {
      username, password
    });
  }
}
