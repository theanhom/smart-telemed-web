
import { NzMessageService } from 'ng-zorro-antd/message';

import { Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { ICreateUser, IUpdateUser } from '../../../../core/model/user';
import { RandomstringService } from '../../../../core/services/randomstring.service';
import { UserService } from '../../services/user.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-modal-new-user',
  templateUrl: './modal-new-user.component.html',
  styleUrls: ['./modal-new-user.component.css']
})
export class ModalNewUserComponent {

  validateForm!: UntypedFormGroup;

  @Output() onSubmit = new EventEmitter<any>();

  isOkLoading = false;
  isVisible = false;
  userId: any = null;
  itemPositions: any;
  selectedPositions: any;

  constructor(
    private randomString: RandomstringService,
    private userService: UserService,
    private message: NzMessageService,
    private fb: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      fname: [null, [Validators.required]],
      lname: [null, [Validators.required]],
      position_id: [null, [Validators.required]],
      status: [true],
      is_admin: [false]
    });

    this.listposition();
  }

  async listposition() {
    const response = await this.userService.getlistposition();
    console.log(response);
    this.itemPositions = response.data;

  }

  showModal(id: any = ''): void {
    this.validateForm.reset()
    this.validateForm.controls['username'].enable()

    this.isVisible = true
    this.userId = null;

    // this.getMainOperators();

    if (id) {
      this.userId = id
      this.getUserInfo(id)
    }

  }

  handleOk(): void {
    if (this.validateForm.valid) {
      if (this.userId) {
        let user: IUpdateUser = {
          username: this.validateForm.value.username,
          fname: this.validateForm.value.fname,
          lname: this.validateForm.value.lname,
          position_id: this.validateForm.value.position_id,
          is_admin: this.validateForm.value.is_admin ? 'true' : 'false',
          status: this.validateForm.value.status ? 'true' : 'false'
        }

        this.doUpdate(user)

      } else {
        let user: ICreateUser = {
          username: this.validateForm.value.username,
          position_id: this.validateForm.value.position_id,
          fname: this.validateForm.value.fname,
          lname: this.validateForm.value.lname,
          is_admin: this.validateForm.value.is_admin ? 'true' : 'false',
          status: this.validateForm.value.status ? 'true' : 'false'
        }

        this.doRegister(user)

      }
      return
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty()
          control.updateValueAndValidity({ onlySelf: true })
        }
      });
      return
    }
  }

  handleCancel(): void {
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
  }

  async doRegister(user: ICreateUser) {
    this.isOkLoading = true
    const messageId = this.message.loading('กำลังบันทึกข้อมูล...').messageId
    try {
      await this.userService.save(user)
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      this.onSubmit.emit(true)
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId)
      this.message.error(`${error.code} - ${error.message}`)
    }
  }

  async doUpdate(user: IUpdateUser) {
    this.isOkLoading = true
    const messageId = this.message.loading('กำลังบันทึกข้อมูล...', { nzDuration: 0 }).messageId
    try {
      await this.userService.update(this.userId, user)
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      this.onSubmit.emit(true);
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  randomPassword() {
    const randomPassword = this.randomString.generateRandomString();
    this.validateForm.patchValue({ password: randomPassword });
  }

  onChangeMainOperator(event: any) {
    // this.getOperators(event);
  }

  async getUserInfo(id: any) {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response: any = await this.userService.info(id);

      const user = response.data.data[0];
      console.log(user);

      this.validateForm.patchValue({
        username: user.username,
        fname: user.fname,
        lname: user.lname,
        position_id: user.position_id,
        is_admin: user.is_admin ? true : false,
        status: user.status,
      })

      // this.getOperators(user.main_operator_id)
      // patch value
      this.validateForm.patchValue({
        fname: user.fname,
      })

      this.message.remove(messageId)
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

}
