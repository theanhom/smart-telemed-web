import { Component } from '@angular/core';

@Component({
  selector: 'app-register-patient',
  templateUrl: './register-patient.component.html',
  styleUrls: ['./register-patient.component.css']
})
export class RegisterPatientComponent {
  panels = [
    {
      active: false,
      name: 'ที่อยู่จัดส่ง (ระบุหากเลือกรับยาทางไปรษณีย์)',
      disabled: false
    }
  ];
  radioValue = "walkin";
}