import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterPatientRoutingModule } from './register-patient-routing.module';
import { RegisterPatientComponent } from './register-patient.component';

import { NgZorroModule } from '../../ng-zorro.module';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';

@NgModule({
  declarations: [
    RegisterPatientComponent
  ],
  imports: [
    NzCollapseModule,
    NzRadioModule,
    NzButtonModule,
    NzDatePickerModule,
    NzSpaceModule,
    NzModalModule,
    NzGridModule,
    NzInputModule,
    NzFormModule,
    NzIconModule,
    NzPageHeaderModule,
    NzTypographyModule,
    NzInputNumberModule,
    NgZorroModule,
    CommonModule,
    RegisterPatientRoutingModule
  ]
})
export class RegisterPatientModule { 

}
