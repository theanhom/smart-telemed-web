import { Component } from '@angular/core';
import { VariableShareService } from '../../core/services/variable-share.service';
import { Subscription } from 'rxjs';
import { NzMenuThemeType } from 'ng-zorro-antd/menu';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  isCollapsed: boolean = false;
  private subscription?: Subscription;
  user_login_name: any;
  user_id: any
  // theme: string = 'light';
  theme: NzMenuThemeType = 'light';

  constructor(
    private variableShareService: VariableShareService
  ) {
    console.log('register');
    this.subscription = this.variableShareService.sharedData$.subscribe(value => {
      this.isCollapsed = value;

    });

    this.subscription = this.variableShareService.sharedDataTheme$.subscribe(value => {
      this.theme = value;
    });
  }
  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
  closeSidebar(): void {
    this.isCollapsed = !this.isCollapsed;
  }
  executeParentMethod(): void {
    // This method will be triggered when the child emits the event
    this.closeSidebar();
    console.log('Parent method executed.');
  }

  getCloseSidebar(): void {
    this.isCollapsed = this.variableShareService.getCloseSidebar();
    console.log(this.isCollapsed);
  }

  getTheme(): void {
    this.theme = this.variableShareService.getTheme();
    console.log(this.theme);
  }

}
