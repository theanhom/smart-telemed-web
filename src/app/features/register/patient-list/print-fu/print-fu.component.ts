import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RegisterhimService } from '../../../register/services/registerhim.service'
import { DateTime } from 'luxon';
import * as moment from 'moment';

@Component({
  selector: 'app-print-fu',
  templateUrl: './print-fu.component.html',
  styleUrls: ['./print-fu.component.css']
})
export class PrintFuComponent {
  jwtHelper: JwtHelperService = new JwtHelperService();
  items: any;
  zipcode: any;
  itemFu: any
  date_serve: any;
  constructor(
    private router: Router,
    private registerhimService: RegisterhimService,
  ) {
    const token: any = sessionStorage.getItem('token');
    const decoded: any = this.jwtHelper.decodeToken(token);
  }

  showZipcode: any;

  async ngOnInit() {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      let i: any = sessionStorage.getItem('itemStorages')
      this.items = JSON.parse(i);
      console.log(this.items);
      this.date_serve = moment(this.items.date_serve).format('YYYY-MM-DD'),
        await this.getAppointment(this.items.hn)
    }
  }
  async getAppointment(hn: any) {

    try {
      const response: any = await this.registerhimService.PreOrderAppoint(hn);
      console.log(response);
      this.itemFu = response.result.filter((item: { date_appoint: string }) => {
        return item.date_appoint === `${this.date_serve}`;
      });
      console.log(this.itemFu);

    } catch (error: any) {

    }

  }
  print() {
    window.print();
  }

}
