import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-consent-form',
  templateUrl: './consent-form.component.html',
  styleUrls: ['./consent-form.component.css']
})
export class ConsentFormComponent {

  print() {
    window.print();
  }

  jwtHelper: JwtHelperService = new JwtHelperService();
  items: any;
  zipcode: any;
  itemFu: any
  date_serve: any;
  constructor(
    private router: Router,
  ) {
    const token: any = sessionStorage.getItem('token');
    const decoded: any = this.jwtHelper.decodeToken(token);
  }

  showZipcode: any;

  async ngOnInit() {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      let i: any = sessionStorage.getItem('itemStoragesPrint')
      this.items = JSON.parse(i);
      console.log(this.items);
    }
  }

  back() {
    this.router.navigate(['/register/patient-list']);
  }
}
