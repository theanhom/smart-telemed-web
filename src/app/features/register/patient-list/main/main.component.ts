import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DateTime } from 'luxon';
import * as _ from 'lodash';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RegisterService } from '../../services/register.service'
import { ModalNewUserComponent } from '../modals/modal-new-user/modal-new-user.component';
import { ModalViewUserComponent } from '../modals/modal-view-user/modal-view-user.component'
import { ModalAppointAddComponent } from '../modals/modal-appoint-add/modal-appoint-add.component'
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { RegisterhimService } from '../../../register/services/registerhim.service'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  @ViewChild(ModalNewUserComponent) private mdlNewUser!: ModalNewUserComponent;
  @ViewChild(ModalViewUserComponent) private mdlViewUser!: ModalViewUserComponent;
  @ViewChild(ModalAppointAddComponent) private mdlAppointAdd!: ModalAppointAddComponent;
  @Output() onSubmit = new EventEmitter<any>();

  jwtHelper: JwtHelperService = new JwtHelperService();
  isOkLoading = false;
  isVisible = false;
  dataWard: any;
  wardName: any;
  wards: any = [];
  query: any;
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  userId: any = '';
  totalInfo: any;
  waittotal: any = 0;
  staff_id: any;
  username_him: any;
  successtotal: any
  dctotal: any;
  datestart: any;
  today: any;
  todaynow: any;
  checkButton: boolean = false;
  disabledTab: boolean = true;
  constructor(
    private router: Router,
    private message: NzMessageService,
    private modal: NzModalService,
    private registerService: RegisterService,
    private registerhimService: RegisterhimService,
  ) {
    const token: any = sessionStorage.getItem('token');
    this.username_him = sessionStorage.getItem('username_him');
    this.user_login_name = sessionStorage.getItem('userLoginName');
  }

  async ngOnInit() {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      // await this.getStaffID();
      await this.getList();
    }
  }

  async getStaffID() {
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response: any = await this.registerService.getInfoStaff(this.username_him);
      this.staff_id = response.data.data[0].id;
      this.message.remove(messageId)
      this.isOkLoading = false
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async onChange(result: Date) {
    const messageId = this.message.loading('Loading...').messageId;
    try {

      if (result) {
        let startDate = DateTime.fromJSDate(result).setLocale('en').toFormat('yyyy-MM-dd');
        let sdate: any = result
        sdate = DateTime.fromJSDate(sdate).setLocale('en').toFormat('yyyy-MM-dd');
        const _limit = this.pageSize;
        const _offset = this.offset;
        let datas = {
          'staff_id': this.staff_id,
          'sdate': sdate
        }
        const response = await this.registerService.getPatientByStaffID(datas);
        const data: any = response.data.results.rows;
        this.total = data.total || 1
        // this.waittotal = data.waittotal
        // this.successtotal = data.successtotal
        // this.dctotal = data.dctotal
        this.dataSet = data.map((v: any) => {
          return v;
        });
        this.checkButton = true;
        this.getTotalInfo();
        this.message.remove(messageId);
      } else {
        this.message.remove(messageId);
        this.checkButton = false;
        this.getList();
      }
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getTotalInfo() {

  }

  async openNewUserRegister() {
    this.mdlNewUser.showModal()
  }

  viewUser(i: any) {
    this.mdlViewUser.showModal(i);
  }

  appointAdd(i: any) {
    this.mdlAppointAdd.showModal(i);
    console.log(i);

  }

  async openEdit(data: any) {
    this.mdlNewUser.showModal(data)
  }

  async doSearch() {
    // this.getList();
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    let datas = {
      'staff_id': this.staff_id,
      'hn': this.query
    }
    try {
      const response = await this.registerService.getPatientByHn(datas);
      const data: any = response.data.results.rows;
      this.total = data.total || 1
      this.waittotal = data.waittotal
      this.successtotal = data.successtotal
      this.dctotal = data.dctotal
      this.dataSet = data.map((v: any) => {
        return v;
      });
      this.message.remove(messageId);
      this.checkButton = true;
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
      this.checkButton = false;
    }
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }


  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.datestart = '';
    this.getList();
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      let sdate: any = new Date();
      if (sdate) {
        sdate = DateTime.fromJSDate(sdate).setLocale('en').toFormat('yyyy-MM-dd');
      } else {
        sdate = '';
      }
      const _limit = this.pageSize;
      const _offset = this.offset;
      let datas = {
        'staff_id': this.staff_id,
        'sdate': sdate
      }
      const response = await this.registerService.getPatientByStaffID(datas);

      const data: any = response.data.results.rows;
      if (data.length > 0) {
        this.total = data.total || 1
        this.waittotal = data.waittotal
        this.successtotal = data.successtotal
        this.dctotal = data.dctotal
        this.dataSet = data.map((v: any) => {
          return v;
        });
        this.getTotalInfo();

        this.message.remove(messageId);
        this.checkButton = true;
      } else {
        this.message.remove(messageId);
        this.checkButton = false;
        this.dataSet = [];
      }
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
      this.checkButton = false;
      this.router.navigate(['/login']);
    }
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  onSubmitRegister(event: any) {
    if (event) {
      this.getList()
    }
  }

  cancel() {
    this.getList();
  }

  async doChangeStatus(i: any) {

  }

  async update(i: any, text: string) {
    let data: any = {
      'status': text
    }
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response_update_patient = await this.registerService.update(i.id, data);
      this.getList();
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.dataSet);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "referout");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportexcel(): void //https://jsonworld.com/demo/how-to-export-data-to-excel-file-in-angular-application
  {
    /* pass here the table id */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'export_telemed.xlsx');

  }

  showConfirmRegisterHimpro(i: any): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการลงทะเบียนในระบบ Himpro</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.saveRegisterHimpro(i)
    });
  }

  async saveRegisterHimpro(i: any) {
    this.today = new Date();
    this.todaynow = DateTime.fromJSDate(this.today).setLocale('en').toFormat('yyyy-MM-dd');

    let info: any = {
      "regdate": this.todaynow,
      "hn": i.hn,
      "cid": '',
      "comeIn_code": "IN16",
      "patientType_code": "PT2",
      "typeVisit_code": "TV1",
      "emergencyType_code": "EMER0",
      "ptClass": {
        "insCode": i.ptclass,
        "insNumber": "",
        "exprie_date": "",
        "hosp_main": "",
        "hosp_sub": "",
        "main_class_code": "",
        "sub_class_code": "",
        "main_class_name": "",
        "sub_class_name": "",
        "authen_code": ""
      },
      "registerRoom_code": "CARD1",
      "sendToScrRoom_code": "SCR97",
      "userRegister": this.user_login_name,
      "clinicCode": "10120",
      "phone": i.phone,
      "ae_code": "",
      "re_visit": true
    }

    this.isOkLoading = true
    const messageId = this.message.loading('กำลังบันทึกข้อมูล...').messageId

    try {
      const response: any = await this.registerhimService.save_register(info);
      console.log(response);

      let data: any = {
        'frequency': response.Frequency,
        'regdate': response.Regdate,
        'register_his': true,
        'status': 'ลงทะเบียน Himpro'
      }
      const response_update_patient = await this.registerService.update(i.id, data);
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      this.getList();
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId)
      this.message.error(`${error.code} - ${error.message}`)
    }
  }

  async saveDoctor(i: any) {
    sessionStorage.setItem('itemDoctor', JSON.stringify(i));
    this.router.navigate(['/register/patient-list/doctor']);
  }

  printFu(i: any) {
    sessionStorage.setItem('itemStorages', JSON.stringify(i));
    this.router.navigate(['/register/patient-list/print-fu']);

  }

  consentForm(i: any) {
    sessionStorage.setItem('itemStoragesPrint', JSON.stringify(i));
    this.router.navigate(['/register/patient-list/consent-form']);
  }

  showConfirmUpdateStatus(i: any, text: string): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการปิด Visit</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.updateStatus(i, text)
    });
  }

  async updateStatus(i: any, text: string) {
    let data: any = {
      'status': text
    }
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response_update_patient = await this.registerService.update(i.id, data);
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      await this.getList();
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

}
