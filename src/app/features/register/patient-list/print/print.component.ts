import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent {
  jwtHelper: JwtHelperService = new JwtHelperService();
  items: any;
  zipcode: any;
  constructor(
    private router: Router,
  ) {
    const token: any = sessionStorage.getItem('token');
    const decoded: any = this.jwtHelper.decodeToken(token);
  }

  showZipcode: any;

  async ngOnInit() {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      let i: any = sessionStorage.getItem('itemStorages')
      this.items = JSON.parse(i);
      this.zipcode = this.items.zipcode.split('');
    }
  }

  print() {
    window.print();
  }

}
