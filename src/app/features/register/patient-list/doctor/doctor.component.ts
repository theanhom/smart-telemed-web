import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import * as _ from 'lodash';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RegisterService } from '../../services/register.service'
import { RegisterhimService } from '../../../register/services/registerhim.service'
import * as moment from 'moment';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent {
  selectedDoctor: any;
  itemsDoctors: any = [];
  code: any;
  noteSign: any;
  icd10: any;
  dataSet: any[] = [];
  dataIcd10: any[] = [];
  jwtHelper: JwtHelperService = new JwtHelperService();
  items: any;
  temp: any;
  pulse: any;
  respiration: any;
  hpressure: any;
  lpressure: any;
  weight: any;
  height: any;
  pain_score: any;
  been_sick_month: any = '00';
  been_sick_year: any = '00';
  been_sick_day: any = '00';
  invest_text: any;
  pi_text: any;
  cc: any;
  diag_text: any;
  itemSelected: any[] = [];
  constructor(
    private router: Router,
    private registerhimService: RegisterhimService,
    private registerService: RegisterService,
    private message: NzMessageService,
    private modal: NzModalService,
  ) {
    const token: any = sessionStorage.getItem('token');
    const decoded: any = this.jwtHelper.decodeToken(token);
  }

  async ngOnInit() {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      let i: any = sessionStorage.getItem('itemDoctor')
      this.items = JSON.parse(i);
      // console.log(this.items);
      await this.getDoctor();
    }
  }

  async getDoctor() {
    try {
      const respone: any = await this.registerhimService.LookUpTable('Users');
      this.itemsDoctors = respone.result.filter((item: { position: string }) => {
        return item.position === `001`;
      });
    } catch (error: any) {

    }

  }

  async getNoteSignup() {
    try {
      let data: any = {
        'code': this.code
      }
      const response: any = await this.registerService.getNoteSignup(data);
      // console.log(response);

      if (response.data.results.length) {
        this.noteSign = response.data.results[0].NAME;
        if (this.cc) {
          this.cc = this.cc + ' ,' + this.noteSign
          this.code = null;
        } else {
          this.cc = this.noteSign;
          this.code = null;
        }
      } else {
        if (this.cc) {
          this.cc = this.cc + ' ,' + this.code
          this.code = null;
        } else {
          this.cc = this.code;
          this.code = null;
        }
      }
    } catch (error: any) {
      const messageId = this.message.loading('Loading...').messageId;
      this.message.error('ไม่พบข้อมูล')
      this.message.remove(messageId);
    }

  }

  onChange(event: any) {
    this.selectedDoctor = event;

    console.log(this.selectedDoctor);

  }

  async saveDataToHis() {
    let data_head = {
      "regdate": moment(this.items.regdate).format('YYYY-MM-DD'),
      "hn": this.items.hn,
      "frequency": this.items.frequency,
      "orderno": "",
      "ptclass_code": '',
      "room_order_code": "LAB1",
      "to_room_drug_code": "",
      "to_room_lab_code": "INV1",
      "to_room_xray_code": "",
      "doctor_name": '',
      "order_drugs": [],
      "order_labs": [],
      "order_xrays": [],
      "order_others": []
    }

  }

  showConfirm(): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการบันทึกข้อมูล Vital Sign ใช่ หรือ ไม่</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.save_vital_sign()
    });
  }

  async save_vital_sign() {
    if (this.selectedDoctor) {
      let info: any = {
        "regdate": moment(this.items.regdate).format('YYYY-MM-DD'),
        "hn": this.items.hn,
        "frequency": this.items.frequency,
        "come_in_type": "IN16",
        "pt_type": "PT2",
        "visit_type": "TV1",
        "emergency_type": "EMER0",
        "scr_room_code": "SCR97",
        "inv_room_code": "INV1",
        "clinic_code": "10120",
        "scr": {
          "temp": this.temp.toString(),
          "pulse": this.pulse.toString(),
          "respiration": this.respiration.toString(),
          "hpressure": this.hpressure.toString(),
          "lpressure": this.lpressure.toString(),
          "weight": this.weight.toString(),
          "height": this.height.toString(),
          "pain_score": this.pain_score.toString(),
          "been_sick": this.been_sick_year + '-' + this.been_sick_month + '-' + this.been_sick_day,
          "cc": this.cc
        },
        "inv": {
          "invest_text": this.invest_text,
          "pi_text": this.pi_text,
          "diag_text": this.diag_text,
          "doctor_id": this.selectedDoctor.userlogin,
          "doctor_name": this.selectedDoctor.username
        }
      }
      try {
        const response = await this.registerhimService.save_vital_sign(info);
        const messageId = this.message.loading('Loading...').messageId;
        this.message.success('บันทึกสำเร็จ')
        this.message.remove(messageId);
        console.log(response);
      } catch (error: any) {

      }
    } else {
      const messageId = this.message.loading('Loading...').messageId;
      this.message.error('กรุณาระบุแพทย์')
      this.message.remove(messageId);
    }
  }

  showConfirm_save_diag_opd(): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการบันทึกการวินิจฉัย ใช่ หรือ ไม่</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.save_diag_opd()
    });
  }

  async save_diag_opd() {
    // this.dataIcd10.forEach(async (e: any) => {
    //   let info: any = {
    //     "regdate": moment(e.regdate).format('YYYY-MM-DD'),
    //     "hn": e.hn,
    //     "frequency": e.frequency,
    //     "dxtype": e.dxtype,
    //     "diag": e.diag,
    //     "discription": e.discription
    //   }
    try {
      const response = await this.registerhimService.save_diag_opd(this.dataIcd10);
      console.log(response);

    } catch (error: any) {

    }
    // console.log(info);

    // });
  }

  isVisible = false;

  showModal(): void {
    this.count = 0;
    // this.itemSelected = [];
    this.dataIcd10 = [];
    this.isVisible = true;
  }

  handleOk(): void {
    this.dataIcd10 = this.itemSelected;
    console.log(this.dataIcd10);

    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    this.itemSelected = [];
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  async getLookupFilter() {
    let data = {
      "table": "Diags",
      "filter_keyword": this.icd10
    }
    try {
      const response: any = await this.registerhimService.lookUpByFilter(data);
      if (response.result.length > 0) {
        this.countNumber();
        console.log(response);
        const messageId = this.message.loading('Loading...').messageId;
        this.message.success('ค้นหาสำเร็จ')
        this.message.remove(messageId);
        this.dataSet = response.result;
        this.showModal();
      }
      else {
        const messageId = this.message.loading('Loading...').messageId;
        this.message.error('ไม่พบข้อมูลที่ค้นหา')
        this.message.remove(messageId);
      }
    } catch (error: any) {
      const messageId = this.message.loading('Loading...').messageId;
      this.message.error('ไม่พบข้อมูลที่ค้นหา')
      this.message.remove(messageId);
    }
  }

  count!: number;

  countNumber() {
    this.count += 1;
    console.log(this.count);

  }


  // getIcd10(i: any) {
  //   this.countNumber();
  //   let info: any = {
  //     "regdate": moment(this.items.regdate).format('YYYY-MM-DD'),
  //     "hn": this.items.hn,
  //     "frequency": this.items.frequency,
  //     "dxtype": this.count,
  //     "diag": i.code,
  //     "discription": i.description
  //   }
  //   this.itemSelected.push(info);
  // }

  async getIcd10(i: any, e: any) {
    // console.log(e);
    // this.countNumber();
    if (e.srcElement.checked) {
      let data: any = {
        "regdate": moment(this.items.regdate).format('YYYY-MM-DD'),
        "hn": this.items.hn,
        "frequency": this.items.frequency,
        "dxtype": this.count,
        "diag": i.code,
        "discription": i.description
      }

      this.itemSelected.push(data);
      console.log(this.itemSelected);

    } else {
      // this.removeItemArray(this.itemSelected, i);
    }
  }


  removeItemArray(array: any, item: any) {
    for (var i in array) {
      if (array[i] == item) {
        array.splice(i, 1);
        break;
      }
    }
  }

  async deleteItem(i: any) {
    console.log(i);

    this.itemSelected.splice(i, 1);
    this.dataIcd10 = this.itemSelected;
    console.log(this.dataIcd10);

  }

}
