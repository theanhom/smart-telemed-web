import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientListRoutingModule } from './patient-list-routing.module';
import { PatientListComponent } from './patient-list.component';
import { MainComponent } from './main/main.component';
import { NgZorroModule } from '../../../ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalChangePasswordComponent } from '../patient-list/modals/modal-change-password/modal-change-password.component';
import { ModalNewUserComponent } from './modals/modal-new-user/modal-new-user.component';
import { ModalViewUserComponent } from './modals/modal-view-user/modal-view-user.component';
import { ModalProfileUserComponent } from './modals/modal-profile-user/modal-profile-user.component';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { SharedModule } from "../../../shared/shared.module";
import { DoctorComponent } from './doctor/doctor.component';
import { PharmacistComponent } from './pharmacist/pharmacist.component';
import { PrintComponent } from './print/print.component';
import { PrintFuComponent } from './print-fu/print-fu.component';
import { ConsentFormComponent } from './consent-form/consent-form.component';
import { ModalAppointAddComponent } from './modals/modal-appoint-add/modal-appoint-add.component';
@NgModule({
  declarations: [
    PatientListComponent,
    MainComponent,
    ModalChangePasswordComponent,
    ModalNewUserComponent,
    ModalViewUserComponent,
    ModalProfileUserComponent,
    DoctorComponent,
    PharmacistComponent,
    PrintComponent,
    PrintFuComponent,
    ConsentFormComponent,
    ModalAppointAddComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    FormsModule,
    ReactiveFormsModule,
    PatientListRoutingModule,
    NzRadioModule,
    SharedModule
  ]
})
export class PatientListModule { }
