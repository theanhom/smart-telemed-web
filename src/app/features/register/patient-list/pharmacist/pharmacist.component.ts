import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DateTime } from 'luxon';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../../shared/services/lib.service';
import { UserProfileService } from '../../../../core/services/user-profiles.service';
import * as _ from 'lodash';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RegisterService } from '../../services/register.service'
import { ModalNewUserComponent } from '../modals/modal-new-user/modal-new-user.component';
import { ModalViewUserComponent } from '../modals/modal-view-user/modal-view-user.component'
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { RegisterhimService } from '../../../register/services/registerhim.service'

@Component({
  selector: 'app-pharmacist',
  templateUrl: './pharmacist.component.html',
  styleUrls: ['./pharmacist.component.css']
})
export class PharmacistComponent {
  @ViewChild(ModalNewUserComponent) private mdlNewUser!: ModalNewUserComponent;
  @ViewChild(ModalViewUserComponent) private mdlViewUser!: ModalViewUserComponent;
  @Output() onSubmit = new EventEmitter<any>();

  jwtHelper: JwtHelperService = new JwtHelperService();
  isOkLoading = false;
  isVisible = false;
  dataWard: any;
  wardName: any;
  wards: any = [];
  query: any;
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  userId: any = '';
  totalInfo: any;
  waittotal: any = 0;
  staff_id: any;
  username_him: any;
  successtotal: any
  dctotal: any;
  datestart: any;
  today: any;
  todaynow: any;
  checkButton: boolean = false;
  disabledTab: boolean = true;
  constructor(
    private router: Router,
    private libService: LibService,
    private message: NzMessageService,
    private modal: NzModalService,
    private userProfileService: UserProfileService,
    private registerService: RegisterService,
    private registerhimService: RegisterhimService,
  ) {
    const token: any = sessionStorage.getItem('token');
    const decoded: any = this.jwtHelper.decodeToken(token);
    this.username_him = sessionStorage.getItem('username_him');
    this.user_login_name = sessionStorage.getItem('userLoginName');
  }

  async ngOnInit() {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      await this.getStaffID();
      await this.getList();
    }
  }

  async getStaffID() {
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response: any = await this.registerService.getInfoStaff(this.username_him);
      this.staff_id = response.data.data[0].id;
      this.message.remove(messageId)
      this.isOkLoading = false
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async onChange(result: Date) {
    const messageId = this.message.loading('Loading...').messageId;
    try {

      if (result) {
        let startDate = DateTime.fromJSDate(result).setLocale('en').toFormat('yyyy-MM-dd');
        console.log('onChange: ', startDate);
        let sdate: any = result
        sdate = DateTime.fromJSDate(sdate).setLocale('en').toFormat('yyyy-MM-dd');
        const _limit = this.pageSize;
        const _offset = this.offset;
        const response = await this.registerService.getPatientByDate(sdate);
        const data: any = response.data;
        if (data.data.length > 0) {
          this.total = data.total || 1
          this.dataSet = data.data.map((v: any) => {
            return v;
          });
          this.checkButton = true;
          this.getTotalInfo();
          this.message.remove(messageId);
        } else {
          this.message.remove(messageId);
          this.checkButton = false;
          this.dataSet = [];
          // this.getList();
        }
      } else {
        this.message.remove(messageId);
        this.checkButton = false;
        this.getList();
      }
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async getTotalInfo() {

  }

  async openNewUserRegister() {
    this.mdlNewUser.showModal()
  }

  viewUser(i: any) {
    this.mdlViewUser.showModal(i);
  }

  async openEdit(data: any) {
    this.mdlNewUser.showModal(data)
  }

  async doSearch() {
    // this.getList();
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    let datas = {
      'staff_id': this.staff_id,
      'hn': this.query
    }
    try {
      const response = await this.registerService.getPatientByHn(datas);
      const data: any = response.data;
      this.total = data.total || 1
      this.dataSet = data.data.map((v: any) => {
        return v;
      });
      this.message.remove(messageId);
      this.checkButton = true;
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
      this.checkButton = false;
    }
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }


  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.datestart = '';
    this.getList();
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      let sdate: any = new Date();
      if (sdate) {
        sdate = DateTime.fromJSDate(sdate).setLocale('en').toFormat('yyyy-MM-dd');
      } else {
        sdate = '';
      }
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.registerService.getPatientByDate(sdate);
      const data: any = response.data;
      if (data.data.length > 0) {
        this.total = data.total || 1
        this.dataSet = data.data.map((v: any) => {
          return v;
        });
        this.getTotalInfo();
        this.message.remove(messageId);
        this.checkButton = true;
      } else {
        this.message.remove(messageId);
        this.checkButton = false;
        this.dataSet = [];
      }
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
      this.checkButton = false;
    }
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  onSubmitRegister(event: any) {
    if (event) {
      this.getList()
    }
  }

  cancel() {
    this.getList();
  }

  async doChangeStatus(i: any) {

  }

  async update(i: any, text: string) {
    let data: any = {
      'status': text
    }
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response_update_patient = await this.registerService.update(i.id, data);
      this.getList();
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.dataSet);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "referout");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  exportexcel(): void //https://jsonworld.com/demo/how-to-export-data-to-excel-file-in-angular-application
  {
    /* pass here the table id */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'export_telemed.xlsx');

  }

  print(i: any) {
    sessionStorage.setItem('itemStorages', JSON.stringify(i));
    this.router.navigate(['/register/patient-list/print']);
  }

}
