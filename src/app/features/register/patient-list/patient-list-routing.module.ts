import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { PatientListComponent } from './patient-list.component';
import { DoctorComponent } from './doctor/doctor.component';
import { PharmacistComponent } from './pharmacist/pharmacist.component';
import { PrintComponent } from './print/print.component';
import { PrintFuComponent } from './print-fu/print-fu.component';
import { ConsentFormComponent } from './consent-form/consent-form.component';

const routes: Routes = [
  {
    path: '',
    component: PatientListComponent,
    data: {
      breadcrumb: 'ทะเบียนผู้ป่วย'
    },
    children: [
      {
        path: '',

        component: MainComponent,
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วย'
        },
      },
      {
        path: 'doctor',

        component: DoctorComponent,
        data: {
          breadcrumb: 'บันทึกข้อมูลการให้บริการ Telemedicine'
        },
      },
      {
        path: 'pharmacist',

        component: PharmacistComponent,
        data: {
          breadcrumb: 'เภสัชกร Telemedicine'
        },
      },
      {
        path: 'print',

        component: PrintComponent,
        data: {
          breadcrumb: 'พิมพ์ Telemedicine'
        },
      },
      {
        path: 'print-fu',

        component: PrintFuComponent,
        data: {
          breadcrumb: 'พิมพ์ Telemedicine'
        },
      },
      {
        path: 'consent-form',

        component: ConsentFormComponent,
        data: {
          breadcrumb: 'พิมพ์ Consent Form'
        },
      },
    ]
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientListRoutingModule { }
