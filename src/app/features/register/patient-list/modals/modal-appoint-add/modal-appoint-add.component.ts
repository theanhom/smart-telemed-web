import { Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';

import { RandomstringService } from '../../../../../core/services/randomstring.service';
import * as _ from 'lodash';
import { RegisterService } from '../../../services/register.service'
import { DateTime } from 'luxon';
import { Router } from '@angular/router';
import { RegisterhimService } from '../../../services/registerhim.service'
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationPlacement, NzNotificationService } from 'ng-zorro-antd/notification';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-appoint-add',
  templateUrl: './modal-appoint-add.component.html',
  styleUrls: ['./modal-appoint-add.component.css']
})
export class ModalAppointAddComponent {
  validateForm!: UntypedFormGroup;

  @Output() onSubmit = new EventEmitter<any>();
  isOkLoading = false;
  isVisible = false;
  userId: any = null;
  query: any;
  cid: any;
  fname: any;
  lname: any;
  phone: any;
  title: any;
  hn: any;
  itemData: any;
  is_active: boolean = false;
  itemTimeServe: any;
  selectedTimeServe: any = null;
  date_serve: any;
  drug_choice: any;
  address: any;
  staff_id: any;
  username_him: any;
  dataSet: any[] = [];
  ptclass: any;
  checkregister: boolean = false;
  zipcode: any;
  age: any;
  sex: any;
  pcucode: any;
  itemPcucode: any;

  createBasicNotification(position: NzNotificationPlacement): void {
    this.notification.blank(
      'แจ้งเตือน',
      'วันและเวลาถูกจองแล้ว  กรุณาเลือกใหม่',
      { nzPlacement: position }
    );
  }

  constructor(
    private randomString: RandomstringService,
    private message: NzMessageService,
    private registerService: RegisterService,
    private router: Router,
    private registerhimService: RegisterhimService,
    private modal: NzModalService,
    private notification: NzNotificationService
  ) {
    this.username_him = sessionStorage.getItem('username_him');
  }

  ngOnInit(): void {
    if (sessionStorage.getItem('token') == '') {
      this.router.navigate(['/login']);
    } else {
      this.getTimeServe();
      // this.getStaffID();
      this.getPcucode();

    }
  }

  async getPcucode() {
    let data = {
      'pcucode': '',
      'pcuname': ''
    }
    let response = await this.registerService.getPcucode(data);
    this.itemPcucode = response.data.results.rows;
  }

  async getStaffID() {
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response: any = await this.registerService.getInfoStaff(this.username_him);
      this.staff_id = response.data.data[0].id;
      this.message.remove(messageId)
      this.isOkLoading = false
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  panels = [
    {
      active: false,
      name: 'ที่อยู่จัดส่ง (ระบุหากเลือกรับยาทางไปรษณีย์)',
      disabled: false
    }
  ];
  radioValue = "walkin";

  showModal(data: any = ''): void {
    console.log(data);

    this.isVisible = true
    this.userId = null;
    this.drug_choice = 'walkin';
    if (data) {
      this.userId = data.id;
      this.hn = data.hn;
      this.cid = data.cid;
      this.fname = data.fname;
      this.lname = data.lname;
      this.phone = data.phone;
      this.drug_choice = data.drug_choice;
      this.address = data.address;
      this.date_serve = data.date_serve;
      this.selectedTimeServe = data.time_serve_id;
      this.staff_id = data.staff_id
      this.ptclass = data.ptclass;
      this.title = data.title;
      this.zipcode = data.zipcode;
      this.age = data.age;
      this.sex = data.ptsex;
      this.pcucode = data.pcucode;
    }
  }

  async getTimeServe() {
    try {
      const response = await this.registerService.time_serve();
      this.itemTimeServe = response.data;
    } catch (error: any) {

    }
  }

  showConfirm(): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการบันทึกข้อมูล</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.handleOk()
    });
  }

  async saveRegister() {

    this.isOkLoading = true
    const messageId = this.message.loading('กำลังบันทึกข้อมูล...').messageId
    try {
      let data: any = {
        'hn': this.hn,
        'cid': this.cid,
        'fname': this.fname,
        'lname': this.lname,
        'phone': this.phone,
        'drug_choice': this.drug_choice,
        'address': this.address,
        'date_serve': moment(this.date_serve).format('YYYY-MM-DD'),
        'time_serve_id': this.selectedTimeServe,
        'staff_id': this.username_him,
        'status': 'ลงทะเบียน',
        'ptclass': this.ptclass,
        'title': this.title,
        'zipcode': this.zipcode,
        'age': this.age,
        'ptsex': this.sex,
        'pcucode': this.pcucode
      }
      console.log(data);

      await this.registerService.saveRegister(data)
      await this.save_fu_himpro();
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      this.onSubmit.emit(true)
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId)
      this.message.error(`${error.code} - ${error.message}`)
    }
    this.clearData();
  }

  handleOk(): void {
    if (this.checkregister) {
      this.createBasicNotification('top')
    } else {
      this.saveRegister();
    }
  }

  handleCancel(): void {
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
    this.clearData();
  }

  clearData() {
    this.itemData = null;
    this.userId = null;
    this.hn = null;
    this.cid = null;
    this.fname = null;
    this.lname = null;
    this.phone = null;
    this.selectedTimeServe = null;
    this.date_serve = null;
    this.drug_choice = null;
    this.address = null;
    this.title = null;
    this.pcucode = null;
  }

  async checkRegisterDuplicate(event: any) {
    let messageId: any
    try {
      if (this.date_serve) {
        this.isOkLoading = true
        messageId = this.message.loading('กำลังตรวจสอบข้อมูล...').messageId
        let date: any = DateTime.fromJSDate(this.date_serve).setLocale('en').toFormat('yyyy-MM-dd');

        const response = await this.registerService.getCheckRegisterDuplicate(date, event);
        if (response.data.data.length) {
          this.createBasicNotification('top')
          this.checkregister = true;
        } else {
          this.checkregister = false;
        }

        this.message.remove(messageId)
        this.isOkLoading = false
      } else {
        this.isOkLoading = true
        messageId = this.message.loading('กรุณาเลือกวันที่รับบริการ...').messageId
        this.isOkLoading = false
      }
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId)
      this.message.error(`${error.code} - ${error.message}`)
    }

  }

  randomPassword() {
    const randomPassword = this.randomString.generateRandomString();
    this.validateForm.patchValue({ password: randomPassword });
  }

  onChangeMainOperator(event: any) {
    // this.getOperators(event);
  }

  async updateStatus(i: any, text: string) {
    let data: any = {
      'status': text
    }
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response_update_patient = await this.registerService.update(i.id, data);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  async save_fu_himpro() {
    let sdate: any = new Date();
    sdate = DateTime.fromJSDate(sdate).setLocale('en').toFormat('yyyy-MM-dd');

    let data: any = {
      "regdate": sdate,
      "hn": this.hn,
      "an": "",
      "frequency": "1",
      "fullname": this.title + this.fname + '  ' + this.lname,
      "appoint_date": DateTime.fromJSDate(this.date_serve).setLocale('en').toFormat('yyyy-MM-dd'),
      "appoint_time": '08:00',
      "appoint_slot": "",
      "from_clinic_code": '10120',
      "from_room_code": "",
      "to_clinic_code": "Null",
      "to_room_code": "SCR97",
      "appoint_for": '',
      "appoint_setpt": "",
      "doctor_name": sessionStorage.getItem('userLoginName'),
      "appoint_by": sessionStorage.getItem('userLoginName')
    }

    try {
      await this.registerhimService.save_fu(data)
    } catch (error: any) {

    }
  }

  itemZipcode: any = [];

  async registerZipcode() {
    let info: any;
    for (let element of this.itemZipcode.data) {
      info = {
        'district': element.district,
        'amphoe': element.amphoe,
        'province': element.province,
        'zipcode': element.zipcode,
        'district_code': element.district_code,
        'amphoe_code': element.amphoe_code,
        'province_code': element.province_code,
      }
      try {
        const response = await this.registerService.registerZipcode(info);
      } catch (error: any) {

      }
    }
  }

  async getZipcode(query: string) {
    try {
      const response = await this.registerService.zipcode(query);
      this.itemZipcode = response.data;
      console.log(response);
    } catch (error: any) {

    }

  }

}

