import { NzMessageService } from 'ng-zorro-antd/message';
import { Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { RandomstringService } from '../../../../../core/services/randomstring.service';
import * as _ from 'lodash';
import { RegisterService } from '../../../services/register.service'
import { DateTime } from 'luxon';
import { NzModalService } from 'ng-zorro-antd/modal';
import { RegisterhimService } from '../../../services/registerhim.service'
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-view-user',
  templateUrl: './modal-view-user.component.html',
  styleUrls: ['./modal-view-user.component.css']
})
export class ModalViewUserComponent {
  validateForm!: UntypedFormGroup;

  @Output() onSubmit = new EventEmitter<any>();
  isOkLoading = false;
  isVisible = false;
  hn: any;
  userId: any = null;
  itemDataContent: any;
  itemData: any;
  today: any;
  todaynow: any;
  user_login_name: any;
  cid: any;
  ptclass: any;
  phone: any;
  id: any;
  register_his!: boolean;
  status: any;
  itemMed: any;

  constructor(
    private randomString: RandomstringService,
    private message: NzMessageService,
    private registerService: RegisterService,
    private fb: UntypedFormBuilder,
    private modal: NzModalService,
    private registerhimService: RegisterhimService,
    private router: Router,

  ) {
    this.user_login_name = sessionStorage.getItem('userLoginName');
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      // hn: [null, [Validators.required]],
    });
  }

  showModal(data: any = ''): void {
    this.validateForm.reset()
    // this.validateForm.controls['hn'].enable()
    this.isVisible = true
    this.userId = null;
    if (data) {
      this.itemData = data;
      this.itemDataContent = data.fname + '  ' + data.lname
      this.id = data.id;
      this.hn = data.hn;
      this.cid = data.cid;
      this.ptclass = data.ptclass;
      this.phone = data.phone;
      this.register_his = data.register_his;
      this.status = data.status;
      this.getPatientMed(data);
    }
  }

  async getPatientMed(i: any) {
    try {
      let data = {
        'hn': i.hn,
        'regdate': moment(i.date_serve).format('YYYY-MM-DD') || moment(i.regdate).format('YYYY-MM-DD'),
        'frequency': i.frequency
      }
      const patientMed = await this.registerService.getPatientMed(data);
      if (patientMed.data.results.rows.length) {
        this.itemMed = await patientMed.data.results.rows;
        console.log(this.itemMed);
      }
    } catch (error: any) {

    }
  }

  handleOk(): void {
    this.isOkLoading = false;
    // this.onSubmit.emit(true);
    this.isVisible = false;
    // if (!this.register_his) {
    //   this.showConfirmRegisterHimpro();
    // } else {
    //   this.isOkLoading = false;
    //   this.onSubmit.emit(true);
    //   this.isVisible = false;
    // }
  }

  handleCancel(): void {
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
  }

  showConfirmRegisterHimpro(): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการลงทะเบียนในระบบ Himpro</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.saveRegisterHimpro()
    });
  }

  async saveRegisterHimpro() {
    this.today = new Date();
    this.todaynow = DateTime.fromJSDate(this.today).setLocale('en').toFormat('yyyy-MM-dd');

    let info: any = {
      "regdate": this.todaynow,
      "hn": this.hn,
      "cid": '',
      "comeIn_code": "IN16",
      "patientType_code": "PT2",
      "typeVisit_code": "TV1",
      "emergencyType_code": "EMER0",
      "ptClass": {
        "insCode": this.ptclass,
        "insNumber": "",
        "exprie_date": "",
        "hosp_main": "",
        "hosp_sub": "",
        "main_class_code": "",
        "sub_class_code": "",
        "main_class_name": "",
        "sub_class_name": "",
        "authen_code": ""
      },
      "registerRoom_code": "CARD1",
      "sendToScrRoom_code": "SCR29",
      "userRegister": this.user_login_name,
      "clinicCode": "10100",
      "phone": this.phone,
      "ae_code": "",
      "re_visit": false
    }

    this.isOkLoading = true
    const messageId = this.message.loading('กำลังบันทึกข้อมูล...').messageId

    try {
      const response = await this.registerhimService.save_register(info);
      let data: any = {
        'register_his': true
      }
      const response_update_patient = await this.registerService.update(this.id, data);
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      this.onSubmit.emit(true)
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId)
      this.message.error(`${error.code} - ${error.message}`)
    }
  }

  showConfirmUpdateStatus(i: any, text: string): void {
    this.modal.confirm({
      nzTitle: '<i>คุณต้องการปิด Visit</i>',
      nzContent: '<b>ใช่ หรือ ไม่ ?</b>',
      nzOnOk: () => this.updateStatus(i, text)
    });
  }

  async updateStatus(i: any, text: string) {
    let data: any = {
      'status': text
    }
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const response_update_patient = await this.registerService.update(i.id, data);
      this.message.remove(messageId)
      this.isOkLoading = false
      this.isVisible = false
      this.onSubmit.emit(true)
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

}
