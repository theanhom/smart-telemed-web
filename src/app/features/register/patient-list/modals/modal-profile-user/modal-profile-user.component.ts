import { NzMessageService } from 'ng-zorro-antd/message';
import { Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { RandomstringService } from '../../../../../core/services/randomstring.service';
import * as _ from 'lodash';
import { RegisterService } from '../../../services/register.service'
import { DateTime } from 'luxon';

@Component({
  selector: 'app-modal-profile-user',
  templateUrl: './modal-profile-user.component.html',
  styleUrls: ['./modal-profile-user.component.css']
})
export class ModalProfileUserComponent {
  validateForm!: UntypedFormGroup;

  @Output() onSubmit = new EventEmitter<any>();
  isOkLoading = false;
  isVisible = false;
  hn: any;
  userId: any = null;
  itemDataContent: any;
  itemData: any;

  constructor(
    private randomString: RandomstringService,
    private message: NzMessageService,
    private registerService: RegisterService,
    private fb: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      // hn: [null, [Validators.required]],
    });
  }

  showModal(data: any = ''): void {
    this.validateForm.reset()
    // this.validateForm.controls['hn'].enable()
    console.log(data);

    this.isVisible = true
    this.userId = null;
    if (data) {
      this.itemData = data;
      this.itemDataContent = data.fname + '  ' + data.lname
      console.log(this.itemDataContent);
    }
  }

  handleOk(): void {
    console.log('OK');
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
  }

}
