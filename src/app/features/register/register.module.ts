import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { NgZorroModule } from '../../ng-zorro.module';
import { SharedModule } from '../../shared/shared.module';
import { VariableShareService } from '../../core/services/variable-share.service';

@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    RegisterRoutingModule
  ],
  providers: [VariableShareService]
})
export class RegisterModule { }
