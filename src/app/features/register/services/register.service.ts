import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  // pathPrefixLookup: any = environment.pathPrefixLookup;
  pathPrefixNurse: any = environment.pathPrefixNurse;
  // pathPrefixAuth: any = environment.pathPrefixAuth;

  private axiosInstance = axios.create({
    // baseURL: `${environment.apiUrl}${this.pathPrefixNurse}`
    baseURL: `${this.pathPrefixNurse}`
  })

  constructor() {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }

  async getWaitingInfo(hn: any) {
    const url = `/his-services/waiting-info?hn=${hn}`
    return await this.axiosInstance.get(url)
  }

  // async getNoteSignup(code: any) {
  //   const url = `/his-services/getNoteSignup/${code}`
  //   return await this.axiosInstance.get(url)
  // }

  async getNoteSignup(data: any) {
    const url = `/telemed-his/getNoteSignup`
    return await this.axiosInstance.post(url, data)
  }

  async getPatientHN(data: any) {
    const url = `/telemed-his/getPatientHN`
    return await this.axiosInstance.post(url, data)
  }

  async getPatientCID(data: any) {
    const url = `/telemed-his/getPatientCID`
    return await this.axiosInstance.post(url, data)
  }

  async getPatientMed(data: any) {
    const url = `/telemed-his/getPatientMed`
    return await this.axiosInstance.post(url, data)
  }

  async saveRegister(data: object) {
    return await this.axiosInstance.post('/patient', data)
  }

  async getPatient() {
    const url = `/patient/getpatient`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  // async getPatientByStaffID(staff_id: string, sdate: string) {
  //   const url = `/patient/getPatientByStaffID/${staff_id}/${sdate}`
  //   console.log(url);
  //   return await this.axiosInstance.get(url)
  // }

  async getPatientByStaffID(data: object) {
    const url = `/telemed-patient/getPatientByStaffID`
    return await this.axiosInstance.post(url, data)
  }

  async getPcucode(data: any) {
    const url = `/telemed-lookup/getPcucode`
    return await this.axiosInstance.post(url, data)
  }

  async getPatientByDate(sdate: string) {
    const url = `/patient/getPatientByDate/${sdate}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async getDoctor() {
    const url = `/his-services/getDoctor`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  // async getPatientByHn(hn: string, staff_id: string) {
  //   const url = `/patient/getpatientbyhn/${hn}/${staff_id}`
  //   console.log(url);
  //   return await this.axiosInstance.get(url)
  // }

  async getPatientByHn(data: any) {
    const url = `/telemed-patient/infoByHN`
    return await this.axiosInstance.post(url, data)
  }

  async getTotalInfo(hn: string) {
    const url = `/patient/gettotalinfo/${hn}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async changePassword(id: any, password: any) {
    return await this.axiosInstance.put(`/patient/${id}/change-password`, {
      password
    })
  }

  async update(id: any, data: any) {
    return await this.axiosInstance.put(`/patient/${id}`, {
      data
    })
  }

  async saveRegisterUser(data: object) {
    return await this.axiosInstance.post('/patient/register', data)
  }

  async saveRegisterProfile(data: object) {
    return await this.axiosInstance.post('/patient/registerProfile', data)
  }

  async time_serve() {
    const url = `/patient/time_serve`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async getInfoStaff(username: string) {
    const url = `/patient/infoStaff/${username}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async getCheckRegisterDuplicate(date_serve: string, time_serve_id: string) {
    const url = `/patient/getCheckRegisterDuplicate/${date_serve}/${time_serve_id}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async zipcode(query: string) {
    const url = `/patient/zipcode/${query}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async registerZipcode(data: object) {
    return await this.axiosInstance.post('/patient/registerZipcode', data)
  }

}
