import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterhimService {
  token_him: any = sessionStorage.getItem('token_him');
  httpOptions: any;

  constructor(@Inject('API_URL_HIM') private apiUrlHim: string, private httpClient: HttpClient) {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'token': this.token_him
      })
    };
  }

  async Patient_cid(cid: any) {
    const _url = `${this.apiUrlHim}/Patient?cid=${cid}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async Patient_hn(hn: any) {
    const _url = `${this.apiUrlHim}/Patient?hn=${hn}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async save_register(data: object) {
    const _url = `${this.apiUrlHim}/Register`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async order_lab(data: object) {
    const _url = `${this.apiUrlHim}/OrderOPD`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async get_order_lab(regdate: any, hn: any,) {
    const _url = `${this.apiUrlHim}/OrderOPD?regdate=${regdate}&hn=${hn}&frequency=1`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async save_vital_sign(data: object) {
    const _url = `${this.apiUrlHim}/OpdVisit`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async save_diag_opd(data: object) {
    const _url = `${this.apiUrlHim}/DiagOpd`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async ucsearch(cid: any) {
    const _url = `${this.apiUrlHim}/UCSearch/${cid}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async LookUpTable(tables: any) {
    const _url = `${this.apiUrlHim}/LookUpTable/${tables}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async ucsearch_him(main_code: any, sub_code: any, hosp_main: any, hosp_sub: any) {
    const _url = `${this.apiUrlHim}/UCSearch?main_code=${main_code}&sub_code=${sub_code}&hosp_main=${hosp_main}&hosp_sub=${hosp_sub}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async getOpdVisit(hn: any, d_start: any, d_end: any) {
    const _url = `${this.apiUrlHim}/OpdVisit?hn=${hn}&d_start=${d_start}&d_end=${d_end}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async getOpdVisitDetail(hn: any, regdate: any, frequency: any) {
    const _url = `${this.apiUrlHim}/OpdVisit/${hn}?regdate=${regdate}&frequency=${frequency}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async register_patient(data: object) {
    const _url = `${this.apiUrlHim}/Patient`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async get_lab(hn: any) {
    const _url = `${this.apiUrlHim}/Lab?hn=${hn}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async get_lab_result(type: any, regdate: any, hn: any, frequency: any, orderno: any, item_code: any) {
    const _url = `${this.apiUrlHim}/Lab/${type}?regdate=${regdate}&hn=${hn}&frequency=${frequency}&orderno=${orderno}&item_code=${item_code}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async PreOrderAppoint(hn: any) {
    const _url = `${this.apiUrlHim}/Appoint/${hn}`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
  }

  async save_result_lab(data: object) {
    const _url = `${this.apiUrlHim}/Lab`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async save_fu(data: object) {
    const _url = `${this.apiUrlHim}/Appoint`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

  async lookUpByFilter(data: object) {
    const _url = `${this.apiUrlHim}/LookUpTable`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }

}
