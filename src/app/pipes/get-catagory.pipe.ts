
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'catagoryname'
})

export class CatagotyNamePipe implements PipeTransform {

    transform(cause: string): string {
        if (cause == null) {
            return '';
        }
        let myArray = cause.split(",").map(Number);
        let catagory: any = [
            { catagory_id: 1, catagory_name: 'chronic' },
            { catagory_id: 2, catagory_name: 'จิตเวชและยาเสพติด' },
            { catagory_id: 3, catagory_name: 'Palliative' },
            { catagory_id: 4, catagory_name: 'LTC (Long Term Care)' },
            { catagory_id: 5, catagory_name: 'IMC (Intermidate Care)' },
            { catagory_id: 6, catagory_name: 'เยี่ยมมารดา' },
            { catagory_id: 7, catagory_name: 'เยี่ยมทารก' },
            { catagory_id: 8, catagory_name: 'พัฒนาการเด็ก' },
            { catagory_id: 9, catagory_name: 'โรคติดต่อ' },
            { catagory_id: 10, catagory_name: 'ผู้สูงอายุ' },
            { catagory_id: 11, catagory_name: 'อื่นๆ' }
        ];
        let catagory_name: any = [];
        for (let i = 0; i < myArray.length; i++) {
            for (let j = 0; j < catagory.length; j++) {
                if (myArray[i] == catagory[j].catagory_id) {
                    catagory_name.push(catagory[j].catagory_name);
                }
            }
        }
        return catagory_name.toString();
    }
}

