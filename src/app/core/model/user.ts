export interface UserList {
  user_id: string;
  username: string;
  fname: string;
  lname: string;
  is_admin: string;
  status: string;
  last_login: string;
}

export interface ICreateUser {
  username: string;
  position_id: string;
  fname: string;
  lname: string;
  is_admin: string;
  status: string;
}

export interface IUpdateUser {
  username: string;
  fname: string;
  lname: string;
  position_id: string;
  is_admin: string;
  status: string;
}
