import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { VariableShareService } from '../../core/services/variable-share.service';
import { Subscription } from 'rxjs';
import { NzMenuThemeType } from 'ng-zorro-antd/menu';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  user_login_name: any;
  // theme: string = 'light';
  private subscription?: Subscription;
  theme: NzMenuThemeType = 'light';
  deviceType: string = 'Unknown';

  constructor(
    private router: Router,
    private variableShareService: VariableShareService,
    private breakpointObserver: BreakpointObserver,

  ) {
    this.user_login_name = sessionStorage.getItem('userLoginName');
    this.subscription = this.variableShareService.sharedDataTheme$.subscribe(value => {
      this.theme = value;
    });
    this.checkDevice();
  }

  checkDevice() {
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[Breakpoints.XSmall]) {
          this.deviceType = 'xs';
          console.log(this.deviceType);

        } else if (result.breakpoints[Breakpoints.Small]) {
          console.log(this.deviceType);
          this.deviceType = 'sm';

        } else if (result.breakpoints[Breakpoints.Medium]) {
          console.log(this.deviceType);
          this.deviceType = 'md';

        } else if (result.breakpoints[Breakpoints.Large]) {
          console.log(this.deviceType);
          this.deviceType = 'lg';

        } else {
          console.log(this.deviceType);
          this.deviceType = 'xl';

        }
      }
    });
  }
  // @HostListener('window:resize', ['$event'])


  logOut() {
    sessionStorage.setItem('token', '');
    sessionStorage.setItem('token_him', '');
    return this.router.navigate(['/login']);
  }
  chageThemeDark() {
    this.theme = 'dark';
    this.variableShareService.setTheme('dark');

  }
  chageThemeLight() {
    this.theme = 'light';
    this.variableShareService.setTheme('light');

  }

  users(){
    return this.router.navigate(['/users']);
  }



}
