import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';

import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ThaiDatePipe } from '../pipes/to-thai-date-pipe';
import { ThaiAgePipe } from '../pipes/to-thai-age.pipe';
import { ThaiCidPipe } from '../pipes/to-thai-cid.pipe';

@NgModule({
  declarations: [
    FooterComponent, ThaiDatePipe, ThaiAgePipe, ThaiCidPipe

  ],
  imports: [
    CommonModule,
    RouterModule,
    NzLayoutModule,
    NzMenuModule,
    NgxSpinnerModule
  ],
  exports: [
    FooterComponent,
    ThaiDatePipe,
    ThaiAgePipe,
    ThaiCidPipe,
    NgxSpinnerModule
  ]
})
export class SharedModule { }
