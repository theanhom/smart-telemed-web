export const environment = {
  production: false,
  apiUrl: 'http://192.168.19.55',
  pathPrefixLookup: `:40013/lookup`,
  pathPrefixNurse: `http://localhost:3003/nurse`,
  pathPrefixAuth: `:40010/auth`,
  API_URL_HIM: `https://api.himpro.trakanhospital.go.th:444/api`
};
